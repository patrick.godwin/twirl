#!/usr/bin/env python
__author__ = 'Zoheyr Doctor'
__description__ = 'A tool for plotting animated cartoons of binaries' 
from distutils.core import setup

setup(
        name='Twirl',
        version='0.1',
        url='https://git.ligo.org/zoheyr-doctor/twirl',
        author = __author__,
        author_email = 'zoheyr.doctor@ligo.org',
        description = __description__,
        scripts = [
            'bin/animate_binaries_from_file',
            'bin/make_event_gifs',
            'bin/inspiral_ringdown',
            'bin/binary_orbit',
            ],
        packages = [
            'twirl',
            ],
        py_modules = [
            'twirl.binary',
            'twirl.artists',
            ],
        data_files = [],
        requires = [])

